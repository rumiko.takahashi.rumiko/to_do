-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Січ 24 2019 р., 10:36
-- Версія сервера: 10.1.37-MariaDB-2.cba
-- Версія PHP: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `beejee_test`
--

-- --------------------------------------------------------

--
-- Структура таблиці `status_user`
--

CREATE TABLE `status_user` (
  `user_id` int(11) NOT NULL,
  `stat` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `status_user`
--

INSERT INTO `status_user` (`user_id`, `stat`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 1),
(5, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `text_task` text NOT NULL,
  `stat` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `task`
--

INSERT INTO `task` (`id`, `text_task`, `stat`, `date`) VALUES
(2, 'test update', 1, '2019-01-17 11:56:33'),
(3, 'test', 0, '2019-01-17 12:11:47'),
(4, 'test', 0, '2019-01-17 12:11:55'),
(5, 'test', 0, '2019-01-17 12:20:25'),
(6, 'test', 0, '2019-01-17 12:21:08'),
(7, 'test', 0, '2019-01-17 12:22:45'),
(8, 'test', 0, '2019-01-17 12:23:26'),
(9, 'test', 0, '2019-01-17 12:25:45'),
(10, 'test', 0, '2019-01-17 12:26:19'),
(11, 'test', 0, '2019-01-17 12:26:45'),
(12, 'test', 0, '2019-01-17 12:27:40'),
(13, 'test', 0, '2019-01-17 12:27:44'),
(14, 'test', 0, '2019-01-17 12:28:50'),
(15, 'test', 0, '2019-01-17 12:29:09'),
(16, 'test', 0, '2019-01-17 12:29:36'),
(17, 'test', 0, '2019-01-17 12:30:39'),
(18, 'test', 0, '2019-01-17 12:30:56'),
(19, 'test', 0, '2019-01-17 12:31:12'),
(20, 'test', 0, '2019-01-17 12:31:39'),
(21, 'test', 0, '2019-01-17 12:32:15'),
(22, 'test', 0, '2019-01-17 12:32:36'),
(23, 'test', 0, '2019-01-17 12:37:29'),
(24, 'фывафыв', 0, '2019-01-17 12:37:34'),
(25, 'asdf', 0, '2019-01-17 13:14:29'),
(27, 'asdf', 0, '2019-01-17 13:14:44'),
(28, 'test', 0, '2019-01-17 13:14:47'),
(29, 'asdf', 0, '2019-01-17 13:15:10'),
(30, 'asdf', 0, '2019-01-17 13:15:14'),
(31, 'test', 0, '2019-01-17 13:15:18'),
(32, 'фывафыв', 0, '2019-01-17 13:15:37'),
(33, 'asdf', 0, '2019-01-17 13:15:41'),
(34, 'asdf', 0, '2019-01-17 13:15:43'),
(35, '1', 0, '2019-01-17 13:15:50'),
(36, '2', 0, '2019-01-17 13:15:56'),
(37, '111', 0, '2019-01-17 13:16:17'),
(38, '1111', 1, '2019-01-17 13:16:21'),
(39, '3', 0, '2019-01-17 13:16:47'),
(40, '3', 0, '2019-01-17 13:16:55'),
(41, '4', 0, '2019-01-17 13:17:21'),
(42, '4', 0, '2019-01-17 13:17:40'),
(74, 'test guest', 0, '2019-01-17 14:33:38'),
(75, 'new', 1, '2019-01-18 08:08:03'),
(76, 'test update', 1, '2019-01-18 08:39:34'),
(77, 'asdfff', 0, '2019-01-18 09:08:27'),
(78, 'gfdhjhj', 0, '2019-01-18 09:08:34'),
(80, 'guest', 1, '2019-01-18 09:10:49'),
(82, 'adsf', 0, '2019-01-18 10:28:17'),
(83, 'admin task', 0, '2019-01-18 10:28:21'),
(84, 'adsf', 0, '2019-01-18 10:51:22'),
(85, '123', 1, '2019-01-18 11:00:55'),
(87, 'Add you Task BeeJee))))', 0, '2019-01-18 13:06:03'),
(88, 'The best task manager in the world!!!!', 1, '2019-01-18 13:07:00'),
(89, 'Yes, It\'s best task manager!!! ', 0, '2019-01-18 13:10:04'),
(90, 'If you read this, add you task)))', 0, '2019-01-18 13:10:50'),
(91, 'fghfhgjhgjgj', 0, '2019-01-21 08:13:27'),
(92, 'vcbvcnn', 0, '2019-01-21 08:14:09'),
(93, '111111111111111', 0, '2019-01-21 08:23:10');

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `login`, `email`, `password`) VALUES
(1, 'guest', 'none', '1'),
(2, 'budnik', 'budnik@gmail.com', '11111111'),
(3, 'budnik', 'vasyl@gmailc.om', '11111111'),
(4, 'admin', 'admin', '123'),
(5, 'new', 'new@mail.com', '11111111'),
(6, 'lol', 'lol@mail.com', '11111111'),
(7, 'lol', '123', '123'),
(8, 'vasyl', 'login@gmail.com', '11111111'),
(9, 'abcdfghfh', 'zwxy@gmail.com', '123'),
(10, 'zwxt', 'abcdef', '123'),
(11, 'zwxt', 'loa1696@gmail.com', '123');

-- --------------------------------------------------------

--
-- Структура таблиці `user_task`
--

CREATE TABLE `user_task` (
  `id_task` int(11) NOT NULL,
  `id_user` int(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user_task`
--

INSERT INTO `user_task` (`id_task`, `id_user`) VALUES
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(74, 0),
(75, 5),
(76, 5),
(77, 0),
(78, 0),
(85, 1),
(80, 1),
(82, 4),
(83, 4),
(84, 4),
(87, 4),
(88, 4),
(89, 8),
(90, 1),
(91, 9),
(92, 9),
(93, 11);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
