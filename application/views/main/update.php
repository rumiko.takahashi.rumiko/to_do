<section>
    <h4>Редактировать категорию </h4>
    <br/>
    <div class="col-lg-4">
        <div class="login-form">
            <form action="#" method="post">
                <p>Text</p>
                <input type="text" name="text_task" placeholder="" value="<?php echo $task['text_task']; ?>">
                <p>Status</p>
                <select name="stat">
                    <option value="1" <?php if ($task['stat'] == 1) echo ' selected="selected"'; ?>>Done!</option>
                    <option value="0" <?php if ($task['stat'] == 0) echo ' selected="selected"'; ?>>On work</option>
                </select>
                <br><br>
                <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
            </form>
        </div>
    </div>
</section>