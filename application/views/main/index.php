<section>
    <form action="#" method="post" >
            <label for="comment">Enter you task</label>
            <input type="textarea" name="task" placeholder="" value="">
        <input  class="btn btn-default" type="submit" name="submit" value="Add">
        <br/><br/>
    </form>
</section>
<br>

<section>

    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Sort
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li class="dropdown-header">Sort by: <?php echo $vars['typeSort'];?></li>
            <li><a href="/login-asc">Login - Low to High</a></li>
            <li><a href="/login-desc">Login - High to Low</a></li>
            <li><a href="/email-asc">E-mail - Low to High</a></li>
            <li><a href="/email-desc">E-mail - High to Low</a></li>
            <li><a href="/date-asc">Date - Low to High</a></li>
            <li><a href="/date-desc">Date - High to Low</a></li>
            <li><a href="/status-asc">Status - On work to Done</a></li>
            <li><a href="/status-desc">Status - Done to On work</a></li>
            <li class="divider"></li>
        </ul>
    </div>
    <br>
</section>
<section>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">login</th>
                <th scope="col">E-mail</th>
                <th scope="col">Text task</th>
                <th scope="col">Status</th>
                <th scope="col">Data</th>
                <th scope="col">Redaction</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vars['task'] as $task => $value): ?>
                <tr>
                    <td scope="row"><?php echo $value['login']; ?></td>
                    <td scope="row"><?php echo $value['email']; ?></td>
                    <td scope="row"><?php echo $value['text_task']; ?></td>
                    <?php if($value['stat']): ?>
                        <td scope="row">Done!</td>
                    <?php else: ?>
                        <td scope="row">On work</td>
                    <?php endif; ?>
                    <td scope="row"><?php echo $value['date']; ?></td>
                    <td scope="row"><a href="/update/<?php echo $value['id'];?>" title="Redaction"><i>R</i></a></td>
                    <td scope="row"><a href="/delete/<?php echo $value['id']; ?>" title="Delete"><i>D</i></a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>
<section>
    <div class="clearfix">
        <?php echo $vars['pagination']; ?>
    </div>
</section>
