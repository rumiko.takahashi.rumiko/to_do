<section>
    <h3>You task list</h3>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">login</th>
            <th scope="col">E-mail</th>
            <th scope="col">Text task</th>
            <th scope="col">Status</th>
            <th scope="col">Data</th>
            <th scope="col">Redaction</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($vars['task'] as $task => $value): ?>
            <tr>
                <td scope="row"><?php echo $value['login']; ?></td>
                <td scope="row"><?php echo $value['email']; ?></td>
                <td scope="row"><?php echo $value['text_task']; ?></td>
                <?php if($value['stat']): ?>
                    <td scope="row">Done!</td>
                <?php else: ?>
                    <td scope="row">On work</td>
                <?php endif; ?>
                <td scope="row"><?php echo $value['date']; ?></td>
                <td scope="row"><a href="/update/<?php echo $value['id'];?>" title="Redaction"><i>R</i></a></td>
                <td scope="row"><a href="/delete/<?php echo $value['id']; ?>" title="Delete"><i>D</i></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</section>