<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/9/18
 * Time: 9:01 PM
 */

namespace application\controller;

use application\core\Controller;

use application\core\Pagination;
use application\models\Task;
use application\models\User;

class MainController extends Controller
{
    public function indexAction()
    {

        if (isset($_POST['submit']) && isset($_POST['task']) && strlen($_POST['task']) > 0) {
            $option['task'] = $_POST['task'];

            if (isset($_SESSION['user'])) {
                $option['userId'] = User::getUserId();
            } else {
                $option['userId'] = 1;
            }

            $taskId = Task::addTask($option);
            Task::setUserStatusTask($option['userId'], $taskId);

            $this->view->redirect('/');
            return true;
        }

        if(!isset($_SESSION['sort'])) {
            $_SESSION['sort'] = 'login-asc';
        }
        $sql = Task::getQueryForSort($_SESSION['sort']);
        $typeSort = Task::typeSort($_SESSION['sort']);

        $taskList = Task::getTask($this->route, $sql);
        $countId = Task::countId();

        $pagination = new Pagination($this->route, $countId, Task::SHOW_BY_DEFAULT);

        if (isset($this->route['id'])) {
            if ($this->route['id'] > ceil(intval($countId / Task::SHOW_BY_DEFAULT))) {
                $this->view->errorCode(404);
            } else {
                $vars = [
                    'task' => $taskList,
                    'typeSort' => $typeSort,
                    'pagination' => $pagination->get(),
                ];
                $this->view->render('TO DO | Main', $vars);
                return true;
            }
        }
        $vars = [
            'task' => $taskList,
            'typeSort' => $typeSort,
            'pagination' => $pagination->get(),
        ];

        $this->view->render('TO DO | Main', $vars);
        return true;
    }

    public function deleteAction ()
    {
        if (isset($_SESSION['user'])) {
            $userOwnerId = Task::getOwnerTask($this->route['id']);
            $userId = User::getUserId();
            $statusUser = User::getStatusUser($userId);

            if ($userOwnerId == $userId || $statusUser['stat'] == 1) {
                Task::deleteTask($this->route['id']);
                $this->view->redirect('/account');
                return true;
            } else {
                $this->view->redirect('/');
                return true;
            }
        } else {
            $this->view->redirect('/');
            return true;
        }
    }


    public function updateAction() {
        if (isset($_SESSION['user'])) {
            $taskId = $this->route['id'];
            $userOwnerId = Task::getOwnerTask($taskId);
            $userId = User::getUserId();
            $statusUser = User::getStatusUser($userId);

            if ($userOwnerId == $userId || $statusUser['stat'] == 1) {

                $oneTask = Task::getOneTask($taskId);

                if(isset($_POST['submit'])) {
                    $task['text_task'] = $_POST['text_task'];
                    $task['stat'] = $_POST['stat'];

                    Task::updateTask($taskId, $task);
                    $this->view->redirect('/account');
                    return true;
                }

                $vars = [
                    'task' => $oneTask,
                ];

                $this->view->render('TO DO | Update', $vars);
                return true;
            } else {
                $this->view->redirect('/');
                return true;
            }
        } else {
            $this->view->redirect('/');
            return true;
        }
    }

    public function sortAction() {
        if (!isset($_SESSION['sort'])) {
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $_SESSION['sort'] = $url;
            $this->view->redirect('/');
            return true;
        } else {
            unset($_SESSION['sort']);
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $_SESSION['sort'] = $url;
            $this->view->redirect('/');
            return true;
        }
    }
}
