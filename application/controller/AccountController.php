<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/10/18
 * Time: 3:40 PM
 */

namespace application\controller;

use application\core\Controller;
use application\models\Task;
use application\models\User;


class AccountController extends Controller
{

    public function indexAction() {

        if (isset($_SESSION['user'])) {
            $userId = User::getUserId();
            $userList = Task::getTaskListUser($userId);
            $vars = [
                'task' => $userList,
            ];
            $this->view->render('TO DO | Account List', $vars);
            return true;
        } else {
            $this->view->redirect('/login');
            return true;
        }
    }

    public function registrationAction()
    {
        if(User::checkLogin() == false){
            $errors = false;
            $vars = [
                'errors' => $errors,
            ];
             if (isset($_POST['submit']) && isset($_POST['email']) && isset($_POST['psw'])
                && isset($_POST['pswrepeat']) && isset($_POST['name'])) {
                 $user['name'] = $_POST['name'];
                 $user['email'] = $_POST['email'];
                 $user['password'] = $_POST['psw'];
                 $user['pswrepeat'] = $_POST['pswrepeat'];

                 if($user['password'] != $user['pswrepeat']){
                     $errors[] = 'password does not match';
                 }
                 if (!User::checkEmail($user['email'])) {
                     $errors[] = 'Not correct email';
                 }
                 if (!User::checkPassword($user['password'])) {
                     $errors[] = 'Not correct password';
                 }
                 if ($errors == false) {
                     if (!User::checkUserData($user['email'], $user['password'])
                         && $user['password'] == $user['pswrepeat']) {
                         $userId = User::registerUser($user);
                         User::auth($userId, $user['name']);
                         User::setStatus($userId);
                         $this->view->redirect('/');
                         return true;
                     }
                 } else {
                     $vars = [
                         'errors' => $errors,
                     ];
                     $this->view->render('TO DO | LOGIN', $vars);
                     return true;
                 }
             }
            $this->view->render('TO DO | Regiser', $vars);
            return true;
        } else {
            $this->view->redirect('/');
            return true;
        }
    }

    public function loginAction() {
        if(User::checkLogin() == false) {
            $passwors = '';
            $email = '';
            $errors = false;
            $vars = [
                'errors' => null,
            ];
            if (isset($_POST['submit']) && isset($_POST['email']) && isset($_POST['password'])) {
                $passwors = $_POST['password'];
                $email = $_POST['email'];

                if ($email == 'admin' && $passwors == '123'){
                    $userId = User::checkUserData($email, $passwors);
                    $userLogin = User::getUserName($userId);
                    User::auth($userId, $userLogin['login']);
                    $this->view->redirect('/');
                } else {
                    if (!User::checkEmail($email)) {
                        $errors[] = 'Not correct email';
                    }
                    if (!User::checkEmailExists($email)) {
                        $errors[] = 'email exists';
                    }
                    if (!User::checkPassword($passwors)) {
                        $errors[] = 'Not correct password';
                    }
                    if ($errors == false) {
                        $userId = User::checkUserData($email, $passwors);
                        $userLogin = User::getUserName($userId);
                        User::auth($userId, $userLogin['login']);
                        $this->view->redirect('/');
                    } else {
                        if (isset($errors)) {
                            $vars = [
                                'errors' => $errors,
                            ];
                            $this->view->render('TO DO | LOGIN', $vars);
                            return true;
                        }
                    }
                }
            }
            $this->view->render('TO DO | LOGIN', $vars);
            return true;
        }
        $this->view->redirect('/account');
        return true;

    }

    public function logoutAction() {
        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            unset($_SESSION['name']);
            unset($_SESSION['sort']);
            $this->view->redirect('/');
            return true;
        }
        $this->view->redirect('/account');
        return true;
    }

}