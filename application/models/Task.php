<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 1/14/19
 * Time: 9:37 PM
 */

namespace application\models;

use application\core\ConnectDb;
use \PDO;

class Task
{

    const SHOW_BY_DEFAULT = 3;

    public static function addTask($option, $status = 0) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "INSERT INTO task (text_task, stat) VALUE (?, ?);";

        $result = $db->prepare($sql);
        $result->execute(array($option['task'], $status));

        if($result) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function getTask($routs, $sql) {
        $max = 3;

        if (isset($routs['id'])){
            $start = ($routs['id'] - 1) * $max;
        } else {
            $start = 0;
        }

        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $result = $db->prepare($sql);
        $result->bindParam(':start', $start, PDO::PARAM_INT);
        $result->bindParam(':end', $max, PDO::PARAM_INT);

        $result->execute();

        $i = 0;
        $task = array();
        while ($row = $result->fetch()){
            $task[$i]['id'] = $row['id'];
            $task[$i]['login'] = $row['login'];
            $task[$i]['email'] = $row['email'];
            $task[$i]['text_task'] = $row['text_task'];
            $task[$i]['stat'] = $row['stat'];
            $task[$i]['date'] = $row['date'];
            $i++;
        }
        return $task;
    }

    public static function getTaskListUser($userId) {

        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && ut.id_user = :userId && t.ID = ut.id_task  ORDER by t.date DESC";

        $result = $db->prepare($sql);
        $result->bindParam(':userId', $userId);
        $result->execute();
        $i = 0;
        $task = array();
        while ($row = $result->fetch()){
            $task[$i]['id'] = $row['id'];
            $task[$i]['login'] = $row['login'];
            $task[$i]['email'] = $row['email'];
            $task[$i]['text_task'] = $row['text_task'];
            $task[$i]['stat'] = $row['stat'];
            $task[$i]['date'] = $row['date'];
            $i++;
        }
        return $task;
    }

    public static function getLastId(){
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT MAX(ID) FROM task';

        $result = $db->prepare($sql);
        $result->execute();
        $id = $result->fetchColumn();
        return $id;
    }

    public static function setUserStatusTask($userId = 0, $taskId) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "INSERT INTO user_task (id_task, id_user) VALUE (?, ?);";

        $result = $db->prepare($sql);
        $result->execute(array($taskId, $userId));
        if($result) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function deleteTask($taskId) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "DELETE task, user_task from task INNER JOIN user_task ON user_task.id_task = task.id WHERE task.id = :taskId";

        $result = $db->prepare($sql);
        $result->bindParam(':taskId', $taskId);
        $result->execute();
        return $result->rowCount();
    }

    public static function getOwnerTask($taskId) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = "SELECT id_user FROM user_task WHERE id_task = :taskId";

        $result = $db->prepare($sql);
        $result->bindParam(':taskId', $taskId);
        $result->execute();
        $id = $result->fetchColumn();
        return $id;
    }

    public static function getOneTask($taskId) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT * FROM task WHERE id=:taskId';

        $result = $db->prepare($sql);
        $result->bindParam(':taskId', $taskId);
        $result->execute();
        $task = $result->fetch();
        return $task;
    }

    public static function updateTask($taskId, $option) {
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'UPDATE task SET text_task=?, stat=? WHERE id=?';

        $result = $db->prepare($sql);
        $result = $result->execute(array($option['text_task'], $option['stat'], $taskId));
        return $result;
    }

    public static function countId(){
        $instance = ConnectDb::getInstance();
        $db = $instance->getConnection();

        $sql = 'SELECT COUNT(ID) FROM task';

        $result = $db->prepare($sql);
        $result->execute();
        $id = $result->fetchColumn();
        return $id;
    }

    public static function getQueryForSort($type)
    {
        switch ($type) {
            case 'login-asc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by u.login ASC LIMIT :end OFFSET :start";
                break;
            case 'login-desc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by u.login DESC LIMIT :end OFFSET :start";
                break;
            case 'date-asc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by t.date ASC LIMIT :end OFFSET :start";
                break;
            case 'date-desc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by t.date DESC LIMIT :end OFFSET :start";
                break;
            case 'email-asc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by u.email ASC LIMIT :end OFFSET :start";
                break;
            case 'email-desc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by u.email DESC LIMIT :end OFFSET :start";
                break;
            case 'status-asc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by t.stat ASC LIMIT :end OFFSET :start";
                break;
            case 'status-desc':
                return "SELECT u.login, u.email, t.id, t.text_task, t.stat, t.date FROM user u, task t INNER JOIN user_task ut WHERE u.ID = ut.id_user && t.ID = ut.id_task ORDER by t.stat DESC LIMIT :end OFFSET :start";
                break;
        }
        return true;
    }

    public static function typeSort($type) {
        switch ($type) {
            case 'login-asc':
                return "Login - Low to High";
                break;
            case 'login-desc':
                return "Login - High to Low";
                break;
            case 'date-asc':
                return "Date - Low to High";
                break;
            case 'date-desc':
                return "Date - High to Low";
                break;
            case 'email-asc':
                return "E-mail - Low to High";
                break;
            case 'email-desc':
                return "E-mail - High to Low";
                break;
            case 'status-asc':
                return "Status - Low to High";
                break;
            case 'status-desc':
                return "Status - High to Low";
                break;
        }
        return true;
    }

}